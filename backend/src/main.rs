use actix_cors::Cors;
use actix_web::{
    get, http, middleware::Logger, post, web, App, Either, Error, HttpResponse, HttpServer,
    Responder, Result,
};
use base64::{engine::general_purpose, Engine as _};
use serde::Deserialize;

#[derive(Deserialize)]
struct Request {
    input: String,
}

#[get("/")]
async fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello world")
}

#[post("/decode")]
async fn decode(request: web::Json<Request>) -> Either<HttpResponse, Result<String, Error>> {
    let decoded_input = match general_purpose::STANDARD_NO_PAD.decode(&request.input) {
        Ok(decoded_input) => decoded_input,
        Err(_) => "Bad input".as_bytes().to_vec(),
    };

    match std::str::from_utf8(&decoded_input) {
        Ok(v) => Either::Right(Ok(String::from(v))),
        Err(_) => Either::Left(HttpResponse::BadRequest().body("Bad input")),
    }
}

#[post("/encode")]
async fn encode(request: web::Json<Request>) -> Result<String> {
    Ok(general_purpose::STANDARD_NO_PAD.encode(&request.input))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    HttpServer::new(move || {
        let cors = Cors::default()
            .allowed_origin("https://tools.corgi.pub")
            .allowed_origin("https://tools.corgi.pub/base64")
            .allowed_origin("http://localhost:5173")
            .allowed_methods(vec!["GET", "POST", "OPTIONS"])
            .allowed_headers(vec![http::header::CONTENT_TYPE, http::header::ACCEPT])
            .max_age(3600);

        App::new()
            .wrap(cors)
            .wrap(Logger::default())
            .service(index)
            .service(decode)
            .service(encode)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
